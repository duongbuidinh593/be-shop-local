import express from "express";
import {
    createProductHandle,
    deleteProductWithIDHandle,
    getAllProductHandle,
    updateProductHandle
} from "../controllers/product.controller";
import {deserializeUser} from "../middleware/deserializeUser";
import {requireUser} from "../middleware/requireUser";
import {requireAdminRole} from "../middleware/requireAdminRole";

const multer = require("multer")

const upload = multer({dest: "../../uploads/"})


const productRouter = express.Router();

productRouter.post('/create_product', deserializeUser, requireUser, requireAdminRole, createProductHandle);

productRouter.get('/', getAllProductHandle);
productRouter.get("/delete/:productId", deserializeUser, requireUser, requireAdminRole, deleteProductWithIDHandle)

productRouter.patch('/update/:productID', deserializeUser, requireUser, requireAdminRole, updateProductHandle)
// upload.fields([{name: "name", maxCount: 1}, {
//     name: "price",
//     maxCount: 1
// }, {name: "description"}, {name: "type"}, {name: "discount"}, {name: "images[]", maxCount: 3}]),

export default productRouter;