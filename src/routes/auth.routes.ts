import express from 'express';
import {
    loginUserHandler,
    logoutHandler,
    refreshAccessTokenHandler,
    registerUserHandler, verifyEmailHandler,
} from '../controllers/auth.controller';
import {deserializeUser} from '../middleware/deserializeUser';
import {requireUser} from '../middleware/requireUser';
import {validate} from '../middleware/validate';
import {createUserSchema, loginUserSchema, verifyEmailSchema} from '../schemas/user.schemas';

const authRouter = express.Router();
authRouter.get(
    '/verifyemail/:verificationCode',
    validate(verifyEmailSchema),
    verifyEmailHandler
);
// Register user
authRouter.post('/register', validate(createUserSchema), registerUserHandler);

// Login user
authRouter.post('/login', validate(loginUserSchema), loginUserHandler);

// Logout user
authRouter.get('/logout', deserializeUser, requireUser, logoutHandler);

// Refresh access token
authRouter.get('/refresh', refreshAccessTokenHandler);


export default authRouter;