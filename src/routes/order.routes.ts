import express from "express";
import {deserializeUser} from "../middleware/deserializeUser";
import {requireUser} from "../middleware/requireUser";
import {
    confirmOrderHandle,
    createOrderHandle,
    getAllOrderByIDHandle,
    getAllOrderHandle
} from "../controllers/order.controller";
import {requireAdminRole} from "../middleware/requireAdminRole";


const orderRouter = express.Router();

orderRouter.post("/create_order", deserializeUser, requireUser, createOrderHandle)

orderRouter.get("/all_orders", deserializeUser, requireUser, getAllOrderByIDHandle)

orderRouter.get("/all_orders_admin", deserializeUser, requireUser, requireAdminRole, getAllOrderHandle)

orderRouter.get("/confirm/:orderNumber", deserializeUser, requireUser, confirmOrderHandle)

export default orderRouter;