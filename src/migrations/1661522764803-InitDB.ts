import {MigrationInterface, QueryRunner, Table} from "typeorm"

export class InitDB1661522764803 implements MigrationInterface {

    public async up(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.createTable(new Table({
            name:"users",
            columns:[{
                name:"username",
                type:"varchar",
                isNullable:false,
                isPrimary:true
            }, {
                    name:"hashed_password",
                    type:"varchar",
                    isNullable:false
            }, {
                name:"full_name",
                type:"varchar",
                isNullable:false
            }, {
                name:"email",
                type:"varchar",
                isNullable:false,
                isUnique:true
            }, {
                name:"roles",
                type:"character varying[]",
                isNullable:false
            },{
                name:"create_at",
                type:"timestamptz",
                isNullable:false,
                default:"now()"
            }
            ]
        }))

    }

    public async down(queryRunner: QueryRunner): Promise<void> {
    }

}
