import {NextFunction, Request, Response} from "express";
import AppError from "../utils/appError";


export const requireAdminRole = (
    req: Request,
    res: Response,
    next: NextFunction
) => {
    try {
        const user = res.locals.user;

        if (user.role !== "admin") {
            return next(
                new AppError(400, `User is not admin!`)
            );
        }

        next();
    } catch (err: any) {
        next(err);
    }
};