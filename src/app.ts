import productRouter from "./routes/product.routes";
import express, {NextFunction, Request, Response} from 'express';
import config from 'config';
import morgan from 'morgan';
import cookieParser from 'cookie-parser';
import cors from 'cors';
import {AppDataSource} from './utils/data-source';
import AppError from './utils/appError';
import authRouter from './routes/auth.routes';
import userRouter from './routes/user.routes';
import validateEnv from './utils/validateEnv';
import cartRouter from "./routes/cart.routes";
import orderRouter from "./routes/order.routes";
import {connectRedis} from "./utils/connectRedis";


require('dotenv').config();


AppDataSource.initialize()
    .then(async () => {
        // VALIDATE ENV
        validateEnv();

        const app = express();

        // TEMPLATE ENGINE
        app.set('view engine', 'pug');
        app.set('views', `${__dirname}/views`);
        app.set("trust proxy", 1);


        // MIDDLEWARE

        // 1. Body parser
        app.use(express.json({limit: '10kb'}));

        // 2. Logger
        if (process.env.NODE_ENV === 'development') app.use(morgan('dev'));

        // 3. Cookie Parser
        app.use(cookieParser());



        // 4. Cors
        app.use(
            cors({
                origin: "http://localhost:3000",
                credentials: true,

            })
        );
        await connectRedis();

        // ROUTES
        app.use('/api/auth', authRouter);
        app.use('/api/users', userRouter);
        app.use('/api/products', productRouter);
        app.use('/api/carts', cartRouter);
        app.use('/api/orders', orderRouter);


        // UNHANDLED ROUTE
        app.all('*', (req: Request, res: Response, next: NextFunction) => {
            next(new AppError(404, `Route ${req.originalUrl} not found`));
        });

        // GLOBAL ERROR HANDLER
        app.use(
            (error: AppError, req: Request, res: Response, next: NextFunction) => {
                error.status = error.status || 'error';
                error.statusCode = error.statusCode || 500;

                res.status(error.statusCode).json({
                    status: error.status,
                    message: error.message,
                });
            }
        );

        const port = config.get<number>('port');
        app.listen(port);

        console.log(`Server started on port: ${port}`);
    })
    .catch((error) => console.log(error));

