import {NextFunction, Request, Response} from "express";
import {CreateOrderInput} from "../schemas/order.schemas";
import {
    confirmOrder,
    createOrder,
    getAllOrder,
    getAllOrderWithUserId,
    getOrderByOrderNumber
} from "../services/order.service";
import {Order} from "../entities/order.entity";
import {OrderDetails} from "../entities/orderDetails.entity";
import {getProductWithID} from "../services/product.service";
import {Product} from "../entities/product.entity";
import {createOrderDetails} from "../services/orderDetails.service";
import Email from "../utils/email";

export const createOrderHandle = async (req: Request<{}, {}, CreateOrderInput>, res: Response, next: NextFunction) => {
    try {
        const {order_address, order_details, customer_name, comments} = req.body
        const user = res.locals.user;
        // Create new order
        const order = new Order()
        order.status = "pending";
        order.orderAddress = order_address;
        order.orderDate = new Date();
        order.fullName = customer_name;
        order.comments = comments;
        const newOrder = await createOrder(order, user.id);

        //Get product;
        //
        order_details.forEach(odt => {
            let product: Product = new Product();
            getProductWithID(+(odt.product_id)).then(value => {
                if (value === null) throw new Error(`Product #${odt.product_id} does not exist!`);
                product = value;
            })

            const orderDetail = new OrderDetails();

            orderDetail.productId = +odt.product_id;
            orderDetail.quantity = +odt.quantity;
            orderDetail.total = +odt.total;
            orderDetail.orderNumber = newOrder.orderNumber;

            createOrderDetails(orderDetail).then(value => {
                return value;
            })
        })

        const orderInfo = await getOrderByOrderNumber(newOrder.orderNumber)
        if(orderInfo !==null){
            res.status(201).json({newOrder: orderInfo})
        }


    } catch (err: any) {
        next(err)
    }
}

export const getAllOrderByIDHandle = async (req: Request, res: Response, next: NextFunction) => {
    try {
        const user = res.locals.user
        const orders = await getAllOrderWithUserId(user.id);
        res.status(200).json({
            orders
        })
    } catch (err: any) {
        next(err)
    }
}

export const getAllOrderHandle = async (req: Request, res: Response, next: NextFunction) => {
    try {
        const allOrders = await getAllOrder();
        res.status(200).json(allOrders);
    } catch (err: any) {
        next(err)
    }
}

export const confirmOrderHandle = async (req: Request, res: Response, next: NextFunction) => {
    try {
        const {orderNumber} = req.params;

        const result = await confirmOrder(+orderNumber);
        const user = res.locals.user;

        try {
            await new Email(user, orderNumber).sendConfirmOrder();

            res.status(201).json({
                status: "success",
                message: `Order #${orderNumber} was confirmed!`
            });
        } catch (err: any) {
            console.log(err)

            return res.status(500).json({
                status: 'error',
                message: `There was an error sending email, please try again: ${err.message}`,
            });
        }


    } catch (err: any) {
        next(err)
    }
}