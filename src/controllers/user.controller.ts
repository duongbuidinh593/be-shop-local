import {NextFunction, Request, Response} from 'express';
import {User} from "../entities/user.entity";

type UserResponse = Omit<User, 'password' | 'verificationCode' | 'orders' | 'hashPassword'>
export const getMeHandler = async (
    req: Request,
    res: Response,
    next: NextFunction
) => {
    try {
        const user = res.locals.user;

        const response: UserResponse = {
            id: user.id,
            verified: user.verified,
            role: user.role,
            created_at: user.created_at,
            email: user.email,
            name: user.name,
            photo: user.photo,
            updated_at: user.updated_at
        }


        res.status(200).status(200).json({

            user: response,

        });
    } catch (err: any) {
        next(err);
    }
};