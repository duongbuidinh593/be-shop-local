import {NextFunction, Request, Response} from "express";

import {
    createProduct,
    deleteProductWithID,
    getAllProduct,
    getProductWithID,
    updateProductWithID
} from "../services/product.service";
import formidable, {Formidable} from "formidable";
import {deleteImage, uploadImage} from "../utils/cloudinary";
import {Product} from "../entities/product.entity";
import {CreateProductInput} from "../schemas/product.schemas";

export const createProductHandle = async (req: Request<{}, {}, CreateProductInput>, res: Response, next: NextFunction) => {
    try {
        const form = formidable({multiples: true})

        form.parse(req, async (err, fields, {files}) => {
            if (err) {
                next(err)
                return
            }
            const product = new Product();
            const {name, price, description, type, discount} = fields;
            if (Array.isArray(files)) {

                product.name = name.toString();
                product.price = +price.toString();
                product.description = description.toString();
                product.type = type.toString();
                product.discount = +discount.toString();
                product.image_urls = [];
                product.public_id_images = [];
                for (const file of files) {
                    if ('filepath' in file) {
                        const result = await uploadImage(file.filepath)
                        product.image_urls.push(result.url);
                        product.public_id_images.push(result.public_id);
                    }
                }
                const newProduct = await createProduct(product);

                res.status(201).json({
                    status: "success",
                    newProduct
                })
            }

            if (!Array.isArray(files)) {

                product.name = name.toString();
                product.price = +price.toString();
                product.description = description.toString();
                product.type = type.toString();
                product.discount = +discount.toString();
                product.image_urls = [];
                product.public_id_images = [];

                if ('filepath' in files) {
                    const result = await uploadImage(files.filepath)
                    product.image_urls.push(result.url);
                    product.public_id_images.push(result.public_id);
                }

                const newProduct = await createProduct(product);

                res.status(201).json({
                    status: "success",
                    newProduct
                })
            }

        })
    } catch (err: any) {
        next(err)
    }
}

export const getAllProductHandle = async (req: Request, res: Response, next: NextFunction) => {
    try {
        const products = await getAllProduct();

        res.status(200).json(products)

    } catch (err: any) {
        next(err)
    }
}

export const deleteProductWithIDHandle = async (req: Request, res: Response, next: NextFunction) => {
    try {
        const {productId} = req.params
        let id = +productId;
        const product = await getProductWithID(id);
        if (product === null) {
            throw new Error("Product does not exist!")
        }
        await deleteImage(product.public_id_images)
        const result = await deleteProductWithID(id);

        res.status(200).json(
            {
                status: "success",
                data: result
            }
        )
    } catch (err: any) {
        next(err)
    }
}

export const updateProductHandle = async (req: Request, res: Response, next: NextFunction) => {
    try {
        const {productID} = req.params;
        const form = new Formidable({multiples: true});


        form.parse(req, async (err, fields, {files}) => {
            const newImageUrls: string[] = []
            const newPublicIdImages: string[] = []
            console.log(files)
            const isChangeImage = files != null

            if (files) {

                // @ts-ignore


                if (!Array.isArray(files)) {
                    if ("filepath" in files) {
                        const result = await uploadImage(files.filepath);
                        newImageUrls.push(result.url);
                        newPublicIdImages.push(result.public_id)
                    }

                } else if (files && Array.isArray(files)) {
                    for (let file of files) {
                        const result = await uploadImage(file.filepath);
                        newImageUrls.push(result.url);
                        newPublicIdImages.push(result.public_id)
                    }

                }
                fields.image_urls = newImageUrls;
                fields.public_id_images = newPublicIdImages;

            }


            const newProduct = await updateProductWithID(+productID, fields,isChangeImage);


            res.status(200).json({newProduct});
        })


    } catch (err: any) {
        next(err)
    }
}