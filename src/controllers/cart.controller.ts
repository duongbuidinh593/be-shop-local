import {NextFunction, Request, Response} from "express";
import {CreateCartInput, UpdateCartInput} from "../schemas/cart.schemas";
import {
    createCart,
    getAllCartWithUserId,
    removeAllCartWithUserId,
    removeCartById,
    updateCartById
} from "../services/cart.service";
import {Product} from "../entities/product.entity";

type createCartResponse = {
    quantity: number,
    userID: string,
    product: Product,
    id: string
}

type cartItemResponse = {
    id: string,
    product_id: number,
    quantity: number,

    name: string,
    price: number,
    type: string,
    description: string,
    discount: number,
    image_urls: string[]
}

type getCartResponse = Omit<createCartResponse, "userID">

export const createCartHandle = async (req: Request<{}, {}, CreateCartInput>, res: Response, next: NextFunction) => {
    try {
        const {product_id, quantity} = req.body;

        const user = res.locals.user;

        const newCart = await createCart({product_id: product_id, quantity: quantity}, user.id)
        const response: cartItemResponse = {
            id: newCart.id,
            product_id: newCart.product.id,
            quantity: newCart.quantity,
            name: newCart.product.name,
            type: newCart.product.type,
            description: newCart.product.description,
            discount: newCart.product.discount,
            image_urls: newCart.product.image_urls,
            price: newCart.product.price

        }

        res.status(201).json({
            status: "success",
            newCart: response
        })

    } catch (err: any) {
        next(err)
    }

}

export const getCartByUserIDHandle = async (req: Request, res: Response, next: NextFunction) => {
    try {
        const user = res.locals.user;

        const carts = await getAllCartWithUserId(user.id);

        const cartsResponse = carts.map(value => {
            const response: cartItemResponse = {
                id: value.id,
                product_id: value.product.id,
                quantity: value.quantity,
                name: value.product.name,
                type: value.product.type,
                description: value.product.description,
                discount: value.product.discount,
                image_urls: value.product.image_urls,
                price: value.product.price

            }
            return response;
        })

        res.status(200).json({
            status: 'success',
            carts: cartsResponse
        })
    } catch (err: any) {
        next(err)
    }
}

export const removeCartWithUserIDHandle = async (req: Request, res: Response, next: NextFunction) => {
    try {
        const user = res.locals.user;

        const response = await removeAllCartWithUserId(user.id)

        res.status(200).json({
            status: "success",
            message: `Remove all cart successfully. ${response.length} cart was removed!`,

        })
    } catch (err: any) {
        next(err)
    }
}

export const updateQuantityCartItem = async (req: Request<{}, {}, UpdateCartInput>, res: Response, next: NextFunction) => {
    try {
        const user = res.locals.user;
        const {cartID, quantity} = req.body;

        const newCart = await updateCartById(cartID, quantity)

        res.status(200).json({
            status: "success",
            newCart

        })
    } catch (err: any) {
        next(err)
    }
}

export const removeCartWithId = async (req: Request, res: Response, next: NextFunction) => {
    try {
        const {cartID} = req.params
        const data = removeCartById(cartID)
        res.status(200).json({
            status: "success",
            message: `Cart #${cartID} was removed`
        })

    } catch (err: any) {
        next(err)
    }

}