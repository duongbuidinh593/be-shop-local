import config from "config";
import {v2 as cloudinary} from 'cloudinary'
import {CloudinaryStorage} from "multer-storage-cloudinary";
import multer from "multer";

const streamifier = require('streamifier')
const cloudinaryConfig = config.get<{
    cloudinaryName: string,
    cloudinaryApiKey: string,
    cloudinaryApiSecretKey: string
}>('cloudinary');
cloudinary.config({
    cloud_name: cloudinaryConfig.cloudinaryName,
    api_key: cloudinaryConfig.cloudinaryApiKey,
    api_secret: cloudinaryConfig.cloudinaryApiSecretKey,

});

// @ts-ignore
// const storage = new CloudinaryStorage({
//     cloudinary,
//     params: {
//         folder: "shop",
//     }
// });.
//
// export const upload = multer({storage: storage})
