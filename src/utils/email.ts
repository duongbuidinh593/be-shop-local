import config from "config";
import {User} from "../entities/user.entity";
import pug from 'pug'

import nodemailer from "nodemailer";
import {OAuth2Client} from "google-auth-library";


const googleMailer = config.get<{
    clientIDMailer: string,
    clientSecretMailer: string,
    refreshTokenMailer: string,
    emailAdminAddress: string
}>('googleMailer')


export default class Email {
    firstName: string;
    to: string;
    from: string;

    constructor(public user: User, public url: string) {
        this.firstName = user.name.split(' ')[0];
        this.to = user.email;
        this.from = `${config.get<string>('emailFrom')}`;
    }


    private async send(template: string, subject: string) {
        const myOAuth2Client = new OAuth2Client(
            googleMailer.clientIDMailer,
            googleMailer.clientSecretMailer
        )
        myOAuth2Client.setCredentials({
            refresh_token: googleMailer.refreshTokenMailer
        })

        const html = pug.renderFile(`${__dirname}/../views/${template}.pug`, {
            firstName: this.firstName,
            subject,
            url: this.url,
        });

        const mailOptions = {

            to: this.to,
            subject,
            html,
        }

        //send mail

        const accessTokenObject = await myOAuth2Client.getAccessToken();
        let accessToken: string;
        if (typeof accessTokenObject.token === 'string') {
            accessToken = accessTokenObject.token;
        } else {
            accessToken = "Error"
        }

        const newTransport = nodemailer.createTransport({
            service: "gmail",
            auth: {
                type: "OAuth2",
                user: googleMailer.emailAdminAddress,
                clientId: googleMailer.clientIDMailer,
                clientSecret: googleMailer.clientSecretMailer,
                refreshToken: googleMailer.refreshTokenMailer,
                accessToken,
            },
        });
        const info = await newTransport.sendMail(mailOptions);

    }

    async sendVerificationCode() {
        await this.send('verificationCode', 'Your account verification code');
    }

    async sendPasswordResetToken() {
        await this.send(
            'resetPassword',
            'Your password reset token (valid for only 10 minutes)'
        );
    }

    async sendConfirmOrder() {
        await this.send(
            'order',
            'Your order was confirmed!'
        )

    }

    // (...)
}
