import {AppDataSource} from "../utils/data-source";
import {Product} from "../entities/product.entity";
import {DeepPartial} from "typeorm";
import {deleteImage} from "../utils/cloudinary";


const productRepository = AppDataSource.getRepository(Product)

export const createProduct = async (input: DeepPartial<Product>) => {
    return await productRepository.save(productRepository.create(input))
}

export const getAllProduct = async () => {
    return await productRepository.find();
}

export const getProductWithID = async (id: number) => {
    return await productRepository.findOneBy({id})
}

export const deleteProductWithID = async (id: number) => {
    return await productRepository.delete({id})
}

export const updateProductWithID = async (id: number, update: any, isChangeImage: boolean) => {
    const product = await getProductWithID(id);


    ///console.log(update);
    if (product === null) {
        throw new Error(`Product with ${id} does not exist!`)
    }
    if (isChangeImage){
        await deleteImage(product.public_id_images);
    }

    Object.assign(product, update);


    return await productRepository.save(product)
}