import config from 'config';
import {DeepPartial} from 'typeorm';
import {User} from '../entities/user.entity';
import {CreateUserInput} from '../schemas/user.schemas';
import redis from '../utils/connectRedis';
import {AppDataSource} from '../utils/data-source';
import {signJwt} from '../utils/jwt';

const userRepository = AppDataSource.getRepository(User);

export const createUser = async (input: DeepPartial<User>) => {
    return userRepository.save(userRepository.create(input));
};

export const findUserByEmail = async ({email}: { email: string }) => {
    return await userRepository.findOneBy({email});
};

export const findUserByVerificationCode = async (verificationCode: string) => {
    return await userRepository.findOneBy({verificationCode: verificationCode})
}

export const findUserById = async (userId: string) => {
    return await userRepository.findOneBy({id: userId});
};

export const findUser = async (query: Object) => {
    return await userRepository.findOneBy(query);
};
export const signTokens = async (user: User) => {
    // 1. Create Session
    redis.set(user.id, JSON.stringify(user), {
        EX: config.get<number>('redisCacheExpiresIn') * 60,
    });

    // 2. Create Access and Refresh tokens
    const access_token = signJwt({sub: user.id}, 'accessTokenPrivateKey', {
        expiresIn: `${config.get<number>('accessTokenExpiresIn')}m`,
    });

    const refresh_token = signJwt({sub: user.id}, 'refreshTokenPrivateKey', {
        expiresIn: `${config.get<number>('refreshTokenExpiresIn')}m`,
    });

    return {access_token, refresh_token};
};