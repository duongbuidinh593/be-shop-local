import {AppDataSource} from "../utils/data-source";
import {Order} from "../entities/order.entity";
import {findUserById} from "./user.service";

const orderRepository = AppDataSource.getRepository(Order)

export const createOrder = async (input: Order, userId: string) => {
    const user = await findUserById(userId);
    if (user === null) {
        throw new Error("User does not exist!");
    }
    input.user = user;
    const newInput = orderRepository.create(input)
    return await orderRepository.save(input);
}


export const getAllOrderWithUserId = async (userId: string) => {
    return await orderRepository.createQueryBuilder('orders').leftJoinAndSelect('orders.orderDetails', 'orderDetails').where('orders.user_id=:id', {id: userId}).getMany();
}

export const getAllOrder = async () => {
    return await orderRepository.createQueryBuilder("orders").leftJoinAndSelect('orders.orderDetails', 'orderDetails').getMany();
}

export const getOrderByOrderNumber = async (orderNumber: number) => {
    return await orderRepository.findOne({where:{orderNumber},relations:["orderDetails"]})
}

export const confirmOrder = async (id: number) => {
    const orderItem = await getOrderByOrderNumber(id);

    if (orderItem === null) {
        throw new Error(`Order with ${id} does not exist!`);
    }

    orderItem.status = "confirmed"

    return await orderRepository.save(orderItem)
}