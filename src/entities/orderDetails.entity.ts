import {Column, Entity, JoinColumn, ManyToOne, OneToOne, PrimaryColumn} from "typeorm";
import {Order} from "./order.entity";
import {Product} from "./product.entity";

@Entity('order_details')
export class OrderDetails {


    @Column({type: "int", name: "quantity", nullable: false})
    quantity: number

    @Column({type: "float", name: "total", nullable: false})
    total: number

    @PrimaryColumn({name: "order_number"})
    orderNumber: number

    @ManyToOne(() => Order)
    @JoinColumn({name: "order_number"})
    order: Order


    @PrimaryColumn({name: "product_id"})
    productId: number

    @ManyToOne(() => Product)
    @JoinColumn({name: "product_id"})
    product: Product

    // constructor(order: Order, product: Product, orderNumber: number, total: number, quantity: number) {
    //     this.order = order;
    //     this.product = product;
    //     this.orderNumber = orderNumber;
    //     this.total = total;
    //     this.quantity = quantity;
    //     this.productId = product.id;
    // }


}