import {Column, Entity, Index, PrimaryGeneratedColumn} from "typeorm";


@Entity('products')
export class Product {
    @PrimaryGeneratedColumn('increment')
    @Index('id_index')
    id: number

    @Column({nullable: false})
    name: string

    @Column({nullable: false, type: "float"})
    price: number

    @Column({nullable: false})
    type: string

    @Column({nullable: false})
    description: string

    @Column({nullable: false, type: "float", default: 0})
    discount: number

    @Column({nullable: false, type: "simple-array"})
    image_urls: string[]

    @Column({nullable: false, type: "simple-array"})
    public_id_images: string[]
}