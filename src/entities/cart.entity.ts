import {Column, Entity, ManyToOne, PrimaryGeneratedColumn} from "typeorm";
import {User} from "./user.entity";
import {Product} from "./product.entity";
import {JoinColumn} from "typeorm";

@Entity('carts')
export class Cart {
    @PrimaryGeneratedColumn('uuid')
    id: string

    @ManyToOne(() => User, (user) => user.id,{nullable:false})
    @JoinColumn({name:"user_id"})
    user: User

    @ManyToOne(() => Product, (product) => product.id,{nullable:false})
    @JoinColumn({name:"product_id"})
    product: Product


    @Column({nullable:false})
    quantity:number




}