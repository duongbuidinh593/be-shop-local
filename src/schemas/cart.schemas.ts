import {number, object, string, TypeOf} from "zod";

export const createCartSchema = object({
    body: object({
        product_id: number({required_error: "ProductId is required"})
        ,
        quantity: number({
            required_error: "Quantity is required!",
            invalid_type_error: "Quantity must be a number!"
        }),
    })
})

export const updateCartSchema  = object({
    body:object({
        cartID: string({required_error: "CartID is required"})
        ,
        quantity: number({
            required_error: "Quantity is required!",
            invalid_type_error: "Quantity must be a number!"
        }).min(1),
    })
})

export type CreateCartInput = TypeOf<typeof createCartSchema>['body']
export type UpdateCartInput = TypeOf<typeof updateCartSchema>['body']